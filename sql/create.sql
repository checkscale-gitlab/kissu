-- CREATE CLIENT TABLE
CREATE TABLE IF NOT EXISTS CLIENT(
	   id integer primary key autoincrement,
	   username VARCHAR UNIQUE NOT NULL,
	   name VARCHAR NOT NULL default 'Ram Bahadur');

CREATE TABLE IF NOT EXISTS SERVICE(
id integer primary key autoincrement,
	   name VARCHAR UNIQUE NOT NULL,
	   client_id INT NOT NULL,
	   port INT NOT NULL UNIQUE,
	   FOREIGN KEY(client_id) REFERENCES CLIENT(ID));
