version: '2'

services:
  moodle:
    image: 'bitnami/moodle:3'
    container_name: `'CLIENT_USERNAME`'_`'SERVICE_NAME`'   # container-name    
    environment:
      - MARIADB_HOST=db
      - MOODLE_DATABASE_USER=bn_moodle
      - MOODLE_DATABASE_NAME=bitnami_moodle
      - ALLOW_EMPTY_PASSWORD=yes
      - MOODLE_USERNAME=CLIENT_USERNAME
      - MOODLE_PASSWORD=PASSWORD
    ports:
      - "`'SERVICE_PORT`':80"             # port-line
    volumes:
      - './moodle_data:/bitnami'
    networks:
      - `'SERVICE_ID`'_`'CLIENT_USERNAME`'_`'SERVICE_NAME`'_network
    depends_on:
      - db
  db:
    image: 'bitnami/mariadb:10.1'
    container_name: `'CLIENT_USERNAME`'_`'SERVICE_NAME`'_db
    environment:
      - MARIADB_USER=bn_moodle
      - MARIADB_DATABASE=bitnami_moodle
      - ALLOW_EMPTY_PASSWORD=yes
    volumes:
      - './mariadb_data:/bitnami'
    networks:
      - `'SERVICE_ID`'_`'CLIENT_USERNAME`'_`'SERVICE_NAME`'_network
      
networks:
  NETWORK_NAME:
      `'SERVICE_ID`'_`'CLIENT_USERNAME`'_`'SERVICE_NAME`'_network
