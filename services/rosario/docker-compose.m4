version: '3.7'

services:
  SERVICE_NAME:
    image: rosariosis/rosariosis
    container_name: SERVICE_NAME   # container-name
    restart: always
    ports:
      - "`'PORT`':80"             # port-line
    environment:
      - ROSARIOSIS_ADMIN_EMAIL=`'CLIENT_USERNAME`'@example.com
      - PGHOST=db
      - PGPASSWORD=`'PASSWORD`'
      - PGDATABASE=`'SERVICE_NAME`'_rosariosis
    links:
      - db:`'SERVICE_NAME`'_db
    volumes:
      - ./extra-addons:/mnt/extra-addons
      - ./odoo:/var/lib/odoo
    networks:
      - NETWORK_NAME

  db:
    image: postgres:10
    container_name: `'SERVICE_NAME`'_db
    volumes:
      - "./postgres/data:/var/lib/postgresql/data"
    environment:
      - POSTGRES_USER=rosario
      - POSTGRES_PASSWORD=`'PASSWORD`'
      - POSTGRES_DB=`'SERVICE_NAME`'_rosariosis
    networks:
      - NETWORK_NAME

networks:
    NETWORK_NAME:
