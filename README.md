# KISSU
<a href="https://www.deviantart.com/umiimou/art/kissu-commission-675905885">
	<img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/17219286/kissu.jpg" alt="KISSU Logo"></img>
</a>

## (K)issu (I)nterface for (S)watantra (S)ervice (U)pkeep

Automate installation and management of docker containers on server.

KISSU helps automate a large portion of management tasks related to services running on servers.

## Dependencies

* [M4](https://www.gnu.org/software/m4/m4.html "M4 Homepage")
* [Bash](https://www.gnu.org/software/bash/ "Bash Homepage")
* [libsqlite3](https://sqlite.org/index.html "SQLite3 Homepage")
* [DMD](https://github.com/dlang/dmd "DMD github page")
* [dub package manager](https://github.com/dlang/dub "dub github page")
* [MAKE](https://www.gnu.org/software/make/ "MAKE Homepage")
* [d2sqlite3](http://biozic.github.io/d2sqlite3/d2sqlite3.html "d2sqlite3 Homepage")
* [painlessjson](https://blackedder.github.io/painlessjson/painlessjson.html "painlessjson documentation")


## USAGE FOR KISSU EXECUTABLE

### INSTALL

```
make
```

### CREATE CLIENT
```
./bin/kissu client create -d <SQLITE3 DATABASE FILE> -u <USERNAME> -n <CLIENT NAME>
```

#### For Help
```
./bin/kissu client create --help
```

### FIND CLIENT
```
./bin/kissu client get -d <SQLITE3 DATABASE FILE> -u <USERNAME>
```

#### For Help
```
./bin/kissu client get --help
```

### UPDATE CLIENT
```
./bin/kissu client update -d <SQLITE3 DATABASE FILE> -u <USERNAME> -n <CLIENT NAME>
```

#### For Help
```
./bin/kissu client update --help
```

### DELETE CLIENT
```
./bin/kissu client delete -d <SQLITE3 DATABASE FILE> -u <USERNAME>
```

#### For Help
```
./bin/kissu client delete --help
```



## Targets for v0.1

  * [ ] Add all the services from old repository
  * [ ] Add script to **start** services
  * [ ] Add script to **stop** services
  * [x] Add script to **create** services in database
  * [x] Add script to **find** services in database 
  * [x] Add script to **update** services from database
  * [x] Add script to **remove** services from database
  * [ ] Add script to **backup** services
  * [X] Add script to **create** client
  * [X] Add script to **find** client
  * [X] Add script to **remove** client
  * [X] Add script to **modify** client information
  * [X] Way to specify default DATABASE file
