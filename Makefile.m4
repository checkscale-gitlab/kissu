build: create-db
	echo auto DATABASE_PATH\= \"`'KISSU_DATA_DIR`'/kissu.sqlite3\"\; > ./src/kissu/source/config.d
	cd src/kissu && $(MAKE)
	mkdir bin || true
	cp src/kissu/kissu bin/kissu

create-db:
	mkdir -p `'KISSU_DATA_DIR`'
	sqlite3 `'KISSU_DATA_DIR`'/kissu.sqlite3 < sql/create.sql

fresh-db: clean create-db

test: clean create-db
	(cd src/kissu && $(MAKE) test)

install: create-db build
	mkdir -p ~/.local/bin
	cp bin/kissu ~/.local/bin/kissu

clean:
	rm `'KISSU_DATA_DIR`'/kissu.sqlite3 || true
