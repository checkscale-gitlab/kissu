build: create-db
	echo auto DATABASE_PATH\= \"$$HOME/.local/kissu/kissu.sqlite3\"\; > ./src/kissu/source/config.d
	cd src/kissu && $(MAKE)
	mkdir bin || true
	cp src/kissu/kissu bin/kissu

create-db:
	mkdir -p ~/.local/kissu
	sqlite3 ~/.local/kissu/kissu.sqlite3 < sql/create.sql

fresh-db: clean create-db

test: clean create-db
	(cd src/kissu && $(MAKE) test)

install: build
	mkdir -p ~/.local/bin
	cp bin/kissu ~/.local/bin/kissu

clean:
	rm ~/.local/kissu/kissu.sqlite3 || true
