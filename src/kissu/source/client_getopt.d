import std.getopt;
import client: Client;
import std.typecons : Nullable, nullable;
import config;
/*
  Returns a nullable client object based on teh arguments
  operation = get|create|update|delete
  rest_args = commandline arguments after operation 
*/
Nullable!Client client_handle_opts(string operation, string[] rest_args) {
  string username, name, database;
  try {
    switch( operation ) {
    case "delete":
    case "get":
      return find_client_handle_opts(rest_args);
    case "update":
    case "create":
      return create_client_handle_opts(rest_args);
    default:
      return Nullable!Client();
    }
  } catch (GetOptException e) {
    import std.stdio: writeln;
    e.msg.writeln;
  }
  return Nullable!Client();
}

auto find_client_handle_opts(string[] args) {
  string username;
  string database = config.DATABASE_PATH;
  
  auto optionsObject = getopt(args,
			      "database|d", "Specify SQLite3 Database File", &database,

			      std.getopt.config.required,
			      "username|u", "Username of Client To Search", &username);
  if (optionsObject.helpWanted)
    {
      defaultGetoptPrinter("KISSU - Create Client Help",
			   optionsObject.options);
      return Nullable!Client();
    }
  return Client.get(username, database);
}

auto create_client_handle_opts(string[] args) {
  string username, name;
  string database = config.DATABASE_PATH;
  auto optionsObject = getopt(args,
			      std.getopt.config.required,
			      "username|u", "Username of new Client", &username,

			      "database|d", "Specify SQLite3 Database File", &database,

			      std.getopt.config.required,
			      "name|n", "Full Name of new Client",   &name);
  if (optionsObject.helpWanted)
    {
      defaultGetoptPrinter("KISSU - Create Client Help",
			   optionsObject.options);
      return Nullable!Client();
    }
  return (new Client(username, name, database)).nullable;
}
