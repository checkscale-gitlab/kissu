import std.getopt;
import service: Service;
import std.typecons : Nullable, nullable;
import client: Client;
import config;
/*
  Returns a nullable service object based on teh arguments
  operation = get|create|update|delete
  rest_args = commandline arguments after operation 
*/

struct ServiceOption{
  Nullable!Service service;
  bool generate_m4;
}

Nullable!ServiceOption service_handle_opts(string operation, string[] rest_args) {
  string username, name;
  string database = config.DATABASE_PATH;
  try {
    switch( operation ) {
    case "delete":
    case "get":
      return find_service_handle_opts(rest_args);
    case "update":
    case "create":{
      return create_service_handle_opts(rest_args);
    }
    default:
      return Nullable!ServiceOption();
    }
  } catch (GetOptException e) {
    import std.stdio;e.msg.writeln;
    return Nullable!ServiceOption();
  }

}

auto find_service_handle_opts(string[] args) {
  string name, client_username;
  bool generate_m4 = false;
  string database = config.DATABASE_PATH;
  
  auto optionsObject = getopt(args,
			      "database|d", "Specify SQLite3 Database File", &database,

			      std.getopt.config.required,
			      "name|n", "Name of service", &name,

			      std.getopt.config.required,
			      "username|u", "Username of Service To Search", &client_username,
			      "m4", "Generate M4 for the service", &generate_m4);
  if (optionsObject.helpWanted)
    {
      defaultGetoptPrinter("KISSU - Create Service Help",
			   optionsObject.options);
      return Nullable!ServiceOption();
    }
  return ServiceOption(Service.get(name, client_username, database), generate_m4).nullable;
}

auto create_service_handle_opts(string[] args) {
  string name, username;
  string database = config.DATABASE_PATH;
  int port;
  auto optionsObject = getopt(args,
			      std.getopt.config.required,
			      "name|n", "Name of new Service", &name,

			      "database|d", "Specify SQLite3 Database File", &database,

			      std.getopt.config.required,
			      "username|u", "Username of client for which to create service", &username,

			      std.getopt.config.required,
			      "port|p", "Port of new Service",   &port);
  if (optionsObject.helpWanted)
    {
      defaultGetoptPrinter("KISSU - Create Service Help",
			   optionsObject.options);
      return Nullable!ServiceOption();
    }

  
  auto nullableClient = Client.get(username, database);
  if (!nullableClient.isNull)
    return ServiceOption(new Service(name, port, nullableClient.get, database).nullable, false).nullable;
  return Nullable!ServiceOption();
}
