string toPrettyJSON(T)(T obj) {
  import std.json;
  JSONValue jsonValue;
  {
    import painlessjson;
    jsonValue = toJSON(obj);
  }
  return jsonValue.toJSON(true);
}

void writePrettyJSON(T)(T obj) {
  import std.stdio: writeln;
  obj.toPrettyJSON.writeln;
}
