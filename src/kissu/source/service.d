import client:Client;
import std.format;
import std.typecons: Nullable, nullable;
import std.conv: to;
import m4Macro;
import config;

class Service {
  enum SAVE_STATES {UPDATED, CREATED, NOT_UPDATED, NOT_CREATED};
  
  @macroName("SERVICE_ID")
  int id;
  @macroName("SERVICE_NAME")
  string name;
  @macroName("SERVICE_PORT")
  int port;

  @macroName("CLIENT")
  @userDefinedField
  Client client;
  private string database;

  this(string name, int port, Client client, string database) {
    this(0, name, port, client, database);
  }
  this(int id, string name, int port, Client client, string database) {
    this.id = id;
    this.name = name;
    this.port = port;
    this.client = client;
    this.database = database;
  }

  static auto all(string database=config.DATABASE_PATH) {
    import d2sqlite3;
    import std.container.array;
    auto db = Database(database);
    Array!Service services;
    
    Statement get_clients_statement =
      db.prepare("SELECT client.username, service.name FROM service join client on service.client_id = client.id");
    // Make Sure Inner_Client Exists
    ResultRange results = get_clients_statement.execute();
    foreach(service_row; results) {
      auto username = service_row.peek!string(0);
      auto name_ = service_row.peek!string(1);
      auto service = Service.get(name_, username, database);
      if (!service.isNull) services.insertBack(service.get);
    }
    return services;
  }
  
  auto save(bool create=true) {
    import d2sqlite3;
    auto db = Database(this.database);
    scope(failure)assert(db.totalChanges == 0, "Operation Performed on failure!");	// check if insertion was carried out
    Service service;
    Statement count_service_statement;    
    if (!create) {
      Nullable!Service nullableService = Service.get(this.name, this.client.username, this.database);
      if (nullableService.isNull) return this.SAVE_STATES.NOT_UPDATED;
      else service = nullableService.get;
      count_service_statement = db.prepare(format("SELECT count(id) FROM service where id = %d",
						  service.id));
    }
    else {
      count_service_statement = db.prepare(format("SELECT count(id) FROM service where id = %d",
						this.id));
    }

    Statement insert_statement = db.prepare("INSERT INTO service (name, port, client_id)
                                    VALUES (:name, :port, :client_id)");
    Statement update_statement = db.prepare("UPDATE service SET port = :port where id = :id");

    // CHECK IF USER ALREADY EXISTS
    auto no_services = count_service_statement.execute().oneValue!int;
    if (create && no_services > 0) return this.SAVE_STATES.NOT_CREATED;
    if (!create && no_services == 0) return this.SAVE_STATES.NOT_UPDATED;
    
    if (!create) {
      // UPDATE ROW
      update_statement.inject(this.port, service.id);
      update_statement.execute();
      return this.SAVE_STATES.UPDATED;
    } else {
      // INSERT ROW
      // check if port exists
      if (db.prepare(format("select count(id) from service where port = %d;", this.port)).execute().oneValue!int == 0)
	insert_statement.inject(this.name, this.port, this.client.id);
      else
	return this.SAVE_STATES.NOT_CREATED;
      return this.SAVE_STATES.CREATED;
    }
  }

  static Nullable!Service get(string name, string client_username, string database) {
    auto nullableClient = Client.get(client_username, database);
    if (nullableClient.isNull) return Nullable!Service();
    Client client = nullableClient.get;

    import d2sqlite3;
    auto db = Database(database);
    
    Nullable!Service service;
    
    Statement count_service_statement =
      db.prepare(format("SELECT count(id) FROM service where name IS '%s' and client_id = %d",
			name, client.id));
    Statement get_service_statement =
      db.prepare(format("SELECT client.username, service.name, service.port, service.id FROM service join client on service.client_id = client.id where service.client_id = %d and service.name = '%s'", client.id, name));
    // Make Sure Inner_Client Exists
    auto count = count_service_statement.execute().oneValue!int;

    if (count == 1){
      ResultRange results = get_service_statement.execute();
      Row service_row = results.front();
      auto username = service_row.peek!string(0);
      auto name_ = service_row.peek!string(1);
      auto port_ = service_row.peek!int(2);
      auto id = service_row.peek!int(3);
      service = (new Service(id, name_, port_, client, database)).nullable;
    }
    return service;
  }
  void remove() {
    assert(this.name, "name is required!");
    assert(this.database, "Database is required!");
    assert(this.port, "Port is required!");
    import d2sqlite3;
    auto db = Database(this.database);
    Statement count_service_statement =
      db.prepare(format("SELECT count(id) FROM service where id = %d",
			this.id));
    assert(count_service_statement.execute().oneValue!int == 1, "Could Not Delete: Service Not Found!");

    Statement delete_statement = db.prepare("DELETE FROM service WHERE id = :id");
    delete_statement.inject(this.id);
    delete_statement.execute();
    assert(db.totalChanges == 1);	// check if insertion was carried out
  }
}
