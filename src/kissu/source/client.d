import std.format;
import std.typecons: Nullable, nullable;
import std.conv: to;
import m4Macro;

class Client {

  enum SAVE_STATES {UPDATED, CREATED, NOT_UPDATED, NOT_CREATED};
  @macroName("CLIENT_USERNAME")
  string username;

  @macroName("CLIENT_NAME")
  string name;
  
  private string database;

  @macroName("CLIENT_ID")
  int id;

  this( string username, string name, string database ) {
    this.username = username;
    this.name=  name;
    this.database = database;
  }

  this (int id, string username, string name, string database) {
    this(username, name, database);
    this.id = id;
  }

  this () {
    this("", "", "");
  }

  /*
    returns False if data was updated
    returns True is data was saved
   */
  auto save(bool create=true) {
    assert(this.username, "Username is required!");
    assert(this.database, "Database is required!");
    import d2sqlite3;
    auto db = Database(this.database);
    scope(failure) assert(db.totalChanges == 0);	// check if insertion was carried out
    Statement count_client_statement =
      db.prepare(format("SELECT count(username) FROM client where username IS '%s'",
			this.username));
    Statement insert_statement = db.prepare("INSERT INTO client (username, name)
                                    VALUES (:username, :name)");
    Statement update_statement = db.prepare("UPDATE client SET name = :name WHERE username = :username ");

    // CHECK IF USER WITH USERNAME ALREADY EXISTS
    auto no_clients = count_client_statement.execute().oneValue!int;
    if (create && no_clients > 0) return this.SAVE_STATES.NOT_CREATED;
    if (!create && no_clients == 0) return this.SAVE_STATES.NOT_UPDATED;
    if (no_clients != 0) {
      // UPDATE ROW
      update_statement.inject(this.name, this.username);
      return this.SAVE_STATES.UPDATED;
    } else {
      // INSERT ROW
      insert_statement.inject(this.username, this.name);
      return this.SAVE_STATES.CREATED;
    }

  }

  void remove() {
    assert(this.username, "Username is required!");
    assert(this.database, "Database is required!");
    import d2sqlite3;
    auto db = Database(this.database);

    Statement count_client_statement =
      db.prepare(format("SELECT count(username) FROM client where username IS '%s'",
			this.username));
    assert(count_client_statement.execute().oneValue!int == 1, "Could Not Delete: Client Not Found!");

    Statement delete_statement = db.prepare("DELETE FROM client WHERE username = :username");
    delete_statement.inject(this.username);
    assert(db.totalChanges == 1);	// check if insertion was carried out
  }

  static Nullable!Client get(string username_, string database_) {
    import d2sqlite3;
    auto db = Database(database_);
    Nullable!Client client;
    Statement count_client_statement =
      db.prepare(format("SELECT count(username) FROM client where username IS '%s'",
			username_));
    Statement get_client_statement =
      db.prepare(format("SELECT id, username, name FROM client where username IS '%s'",
			username_));
    // Make Sure Client Exists
    auto count = count_client_statement.execute().oneValue!int;

    if (count == 1){
      ResultRange results = get_client_statement.execute();
      Row client_row = results.front();
      auto id = client_row.peek!int(0);
      auto uname = client_row.peek!string(1);
      auto name_ = client_row.peek!string(2);
      client = (new Client(id, uname, name_, database_)).nullable;
    }
    return client;
  }
}
