#!/usr/bin/env bash

mkdir -p ~/.config/kissu
mkdir -p ~/.local/kissu/services

{
    echo "KISSU_ROOT=$PWD"
    echo "KISSU_SERVICES_DIR=$HOME/.local/kissu/services"
    echo "KISSU_DATA_DIR=$HOME/.local/kissu"
    echo "KISSU_CONFIG_DIR=$HOME/.config/kissu"
} > ~/.config/kissu/kissu.config

./bin/kissu-generate-makefile > Makefile
make install
